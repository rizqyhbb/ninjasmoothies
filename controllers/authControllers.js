const User = require("../models/User");
const jwt = require("jsonwebtoken");
require("dotenv").config();

// error handler
const erorrHandler = (err) => {
  console.log(err.message, err.code);
  let errors = { email: "", password: "" };

  // duplicate error
  if (err.code === 11000) {
    errors.email = "this email has been registered";
    return errors;
  }

  // validation errors
  if (err.message.includes("user validation failed")) {
    Object.values(err.errors).forEach(({ properties }) => {
      errors[properties.path] = properties.message;
    });
  }
  return errors;
};

// Create Token (JWT)
const maxSession = 3 * 24 * 60 * 60;
const createToken = (id) => {
  return jwt.sign({ id }, process.env.TOKEN_SECRET, { expiresIn: maxSession });
};

module.exports.signup_get = (req, res) => {
  res.render("signup");
};

module.exports.signup_post = async (req, res) => {
  const { email, password } = req.body;

  try {
    const user = await User.create({ email, password });
    const token = createToken(user._id);
    res.cookie("jwt", token, { httpOnly: true, maxSession: maxSession * 1000 });

    res.status(201).json({ user: user._id });
  } catch (err) {
    const errors = erorrHandler(err);
    res.status(400).json({ errors });
  }
};

module.exports.signin_get = (req, res) => {
  res.render("signin");
};

module.exports.signin_post = async (req, res) => {
  const { email, password } = req.body;
  try {
    const user = await User.login(email, password);
    res.status(200).json({ user: user._id });
  } catch (err) {
    res.status(400).json({ message: "error" });
  }
};
