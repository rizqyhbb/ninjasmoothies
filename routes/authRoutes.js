const { Router } = require("express");
const router = Router();
const authController = require("../controllers/authControllers");

// signup routes
router.get("/signup", authController.signup_get);
router.post("/signup", authController.signup_post);

// login routes
router.get("/signin", authController.signin_get);
router.post("/signin", authController.signin_post);

module.exports = router;
